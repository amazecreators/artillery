# clone our repo
# --depth 1 removes all but one .git commit history
git clone --depth 1 https://amazecreators@bitbucket.org/amazecreators/artillery.git

# change directory to our repo
cd automation

# add required global libraries
 npm install -g typings webpack-dev-server concurrently

# install the repo with npm
npm install

# install the artillery with npm
npm install -g artillery

# create configuration json file for env vars
# save in `config/` as `config.json`
{
  "ENV" : "development",
  # Cannot be 8080 as this conflicts with our Webpack dev server
  "PORT" : 3000,
  "MONGO_URI" : {
    "DEVELOPMENT" : "mongodb://[username:password]@host[:port]",
    "PRODUCTION" : "mongodb://[username:password]@host[:port]",
    "TEST" : "mongodb://[username:password]@host[:port]"
  },
  # Generate your own 256-bit WEP key here:
  # http://randomkeygen.com/
  # Note that you don't need to use specifically
  # this, but it will certainly suffice
  "SESSION_SECRET" : "355FC4FE9348639B4E4FED1B8E93C"
}

# build code
npm run build

# start up the stack

# this command runs two commands in parallel via `concurrently`:
# `npm run server` starts up `webpack-dev-server` allowing for
# hot module reloading
# `npm` run watch` uses `webpack` to watch the necessary files
# and build on file change
npm start

# in a separate terminal:
# start `Express` server
gulp serve

# start `Artillery Script`
npm run artillery
```
 
go to <http://0.0.0.0:3000> or <http://localhost:3000> in your browser

# Table of Contents

- [File Structure](#file-structure)
- [Getting Started](#getting-started)

  - [Dependencies](#dependencies)
  - [Installing](#installing)
  - [Running the app](#running-the-app)

- [Contributing](#contributing)
- [TypeScript](#typescript)
- [Typings](#typings)
- [Frequently asked questions](#frequently-asked-questions)
- [Support, Questions, or Feedback](#support-questions-or-feedback)
- [License](#license)

## File Structure

We use the component approach in our starter. This is the new standard for developing Angular apps and a great way to ensure maintainable code by encapsulation of our behavior logic. A component is basically a self contained app usually in a single file or a folder with each concern as a file: style, template, specs, e2e, and component class. Here's how it looks:

```
ng2-mean-webpack/
 │
 ├──app/                            * our source files for back-end routing and MongoDB object modelling
 |   |──artillery/
 |   |   |──automation.yml          * artillery file that run with npm run artillery
 |   |   └──changeStatus.js         * java file used with artillery script
 │   ├──models/                     * model definitions for Mongoose
 │   │   ├──user.model.js           * a user model for use with PassportJS
 |   |   |──comapnies.model.js      * a comapnies model for use with routes
 |   |   └──locations.model.js      * a locations model for use with routes
 │   ├──routes/                     * store all modular REST API routes for Express here
 │   │   |──authentication          * an Express route for use with PassportJS
 |   |   └──companies.router.js     * an Express route for companies model - get, put, delete api
 │   └──routes.js                   * gather all of your Express routes and middleware here
 │
 ├──config/                         * configuration files for environment variables, Mongoose, and PassportJS
 │   ├──config.json/                * allows definition of environment variables
 │   ├──env.conf.js/                * contains utility functions for setting up environment vars
 │   ├──mongoose.conf.js/           * configuration file for Mongoose
 │   └──passport.conf.js/           * configuration file for PassportJS
 │
 ├──sockets/                        * directory for socket.io functionality
 │   └──base.js/                    * a basic socket.io server function
 │
 ├──src/                            * our source files that will be compiled to javascript
 │   ├──main.ts                     * our entry file for our browser environment
 │   │
 │   ├──index.html                  * Index.html: where we generate our index page
 │   │
 │   ├──polyfills.ts                * our polyfills file
 │   │
 │   ├──app/                        * WebApp: folder
 │   │   ├──home/                   * an example component directory
 │   │   │   ├──home.component.ts   * a simple Angular 2 component
 │   │   │   ├──home.html           * template for our component
 │   │   │   └──home.service.ts     * Angular 2 service linking to our API
 │   │   ├──app.spec.ts             * a simple test of components in app.ts
 │   │   ├──app.e2e.ts              * a simple end-to-end test for /
 │   │   └──app.ts                  * App.ts: a simple version of our App component components
 │   │
 │   ├──assets/                     * static assets are served here
 │   │   ├──icon/                   * our list of icons from www.favicon-generator.org
 │   │   ├──service-worker.js       * ignore this. Web App service worker that's not complete yet
 │   │   ├──robots.txt              * for search engines to crawl your website
 │   │   └──human.txt               * for humans to know who the developers are
 │   │
 │   └──sass/                       * folder for Sass stylesheets
 │       |
 │       ├──base/
 │       │   ├──_animations.scss    * Animation keyframe definitions
 │       │   ├──_reset.scss         * Reset/normalize
 │       │   ├──_typography.scss    * Typography rules
 │       │   ├──_module.scss        * Load all partials from this directory into single partial
 │       │   └── …                  * Etc.
 │       │
 │       ├──components/
 │       │   ├──_buttons.scss       * Buttons
 │       │   ├──_carousel.scss      * Carousel
 │       │   ├──_cover.scss         * Cover
 │       │   ├──_dropdown.scss      * Dropdown
 │       │   ├──_module.scss        * Load all partials from this directory into single partial
 │       │   └── …                  * Etc.
 │       │
 │       ├─ layout/
 │       │   ├──_navigation.scss    * Navigation
 │       │   ├──_grid.scss          * Grid system
 │       │   ├──_header.scss        * Header
 │       │   ├──_footer.scss        * Footer
 │       │   ├──_sidebar.scss       * Sidebar
 │       │   ├──_forms.scss         * Forms
 │       │   ├──_module.scss        * Load all partials from this directory into single partial
 │       │   └── …                  * Etc.
 │       │
 │       ├─ pages/
 │       │   ├──_home.scss          * Home specific styles
 │       │   ├──_contact.scss       * Contact specific styles
 │       │   ├──_module.scss        * Load all partials from this directory into single partial
 │       │   └── …                  * Etc.
 │       │
 │       ├─ themes/
 │       │   ├──_theme.scss         * Default theme
 │       │   ├──_admin.scss         * Admin theme
 │       │   ├──_module.scss        * Load all partials from this directory into single partial
 │       │   └── …                  * Etc.
 │       │
 │       ├─ utils/
 │       │   ├──_variables.scss     * Sass Variables
 │       │   ├──_functions.scss     * Sass Functions
 │       │   ├──_mixins.scss        * Sass Mixins
 │       │   ├──_helpers.scss       * Class & placeholders helpers
 │       │   ├──_module.scss        * Load all partials from this directory into single partial
 │       │   └── …                  * Etc.
 │       ├──vendors/
 │       │   ├──_bootstrap.scss     * Bootstrap
 │       │   ├──_jquery-ui.scss     * jQuery UI
 │       │   ├──_module.scss        * Load all partials from this directory into single partial
 │       │   └── …                  * Etc.
 │       │
 │       │
 │       └──main.scss               * Main sass file to load all partials for this project
 │
 ├──.babelrc                        * configure Babel 6 plugins and ES6/ES7 presets
 │
 ├──server.js                       * ES5 `.js` file importing the server code along with a Babel 6 hook to transpile server ES6/ES7 code on the fly
 ├──server.conf.js                  * configure Express/Socket.io application, connect to database, instantiate Mongoose models, define API and front-end Angular routes, et cetera
 │
 ├──gulpfile.js                     * ES5 `gulpfile` importing the `gulp` workflow code along with a Babel 6 hook to transpile the ES6 code on the fly
 ├──gulpfile.conf.js                * contains all of the workflow management delegated to `gulp`: auto documentation generation; `sass` linting; `nodemon`, et cetera
 │
 ├──spec-bundle.js                  * ignore this magic that sets up our angular 2 testing environment
 ├──karma.config.js                 * karma config for our unit tests
 ├──protractor.config.js            * protractor config for our end-to-end tests
 │
 ├──tsconfig.json                   * config that webpack uses for typescript
 ├──typings.json                    * our typings manager
 ├──package.json                    * what npm uses to manage it's dependencies
 │
 ├──webpack.config.js               * our development webpack config
 ├──webpack.test.config.js          * our testing webpack config
 └──webpack.prod.config.js          * our production webpack config
```

# Getting Started

## Dependencies

What you need to run this app:

- `node` and `npm` (`brew install node`)
- Ensure you're running the latest versions Node `v4.1.x`+ and NPM `2.14.x`+

Once you have those, you should install these globals with `npm install --global`:

- `webpack` (`npm install --global webpack`)
- `webpack-dev-server` (`npm install --global webpack-dev-server`)
- `karma` (`npm install --global karma-cli`)
- `protractor` (`npm install --global protractor`)
- `typings` (`npm install --global typings`)
- `typescript` (`npm install --global typescript`)

## Installing

- `fork` this repo
- `clone` your fork
- `npm install` to install all dependencies
- `typings install` to install necessary typings
- create `config.json` **_see below_**
- `npm run build` to build necessary front-end code with Webpack
- `npm start` to enable hot module reloading and build on file change
- In a new terminal, `node server` to start the server for the first time

## config.json

The `server.conf.js` file is expecting certain `environment` `variables` to be set within `Node`. The `env.conf.js` has functions to check whether the expected `environment` `variables` have been setup before proceeding to start up the rest of the server. You can create a file called `config.json` and store it in the `config` directory that looks something like this:

```
{
  "ENV" : "development",
  # MAKE SURE PORT IS NOT 8080 OR WHATEVER THE WEBPACK
  # DEV SERVER PORT IS SET TO
  "PORT" : 3000,
  "MONGO_URI" : {
    "DEVELOPMENT" : "mongodb://[username:password]@host[:port]",
    "PRODUCTION" : "mongodb://[username:password]@host[:port]",
    "TEST" : "mongodb://[username:password]@host[:port]"
  },
  # Generate your own 256-bit WEP key here:
  # http://randomkeygen.com/
  # Note that you don't need to use specifically
  # this, but it will certainly suffice
  "SESSION_SECRET" : "355FC4FE9348639B4E4FED1B8E93C"
}
```

## Running the app

After you have installed all dependencies and created your `config.json` file, you can now run the app. First, you must start up the back-end server in a separate terminal using the `gulp serve` command. This will fire up our Express app using `nodemon`, which will watch for file changes and restart our backend when necessary. Next use the `npm start` command in the original terminal which runs two `npm` scripts in parallel: `npm run server` to start `webpack-dev-server` for building our front-end in the computer's memory, enabling hot module reloadig; `npm run watch` to watch all of the front-end files and build them upon changes. You can now fire up your favorite web browser and visit the running application at `localhost:8080`!

### server

```bash
# development
# package front-end files with Webpack and hot reload
# upon any changes
npm start
# use `Gulp` in a second terminal to run the Express
# app responsible for our back-end
gulp serve
# optionally use `Gulp` in a third terminal to auto
# generate documentation and lint `Sass`
gulp

# production
npm run build:prod
npm run server:prod
```

## Other commands

### start `Express` back-end

```bash
gulp serve
```

### build documentation

```bash
gulp build:docs
```

### watch and build documentation

```bash
gulp watch:docs
```

### watch and lint sass

```bash
gulp watch:sass
```

### build files

```bash
# development
npm run build:dev
# production
npm run build:prod
```

### watch and build files

```bash
npm run watch
```

### run tests

```bash
npm run test
```

### watch and run our tests

```bash
npm run watch:test
```

### run end-to-end tests

```bash
# make sure you have your server running in another terminal
npm run e2e
```

### run webdriver (for end-to-end)

```bash
npm run webdriver:update
npm run webdriver:start
```

### run Protractor's elementExplorer (for end-to-end)

```bash
npm run webdriver:start
# in another terminal
npm run e2e:live
```
# start `Artillery Script`
npm run artillery
