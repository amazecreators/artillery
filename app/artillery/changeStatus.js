// controller for the artillery file
// handles custom scripts
'use strict';

module.exports = {
  get,
  put  
};

// Make sure to "npm install faker" first.
var Faker = require('faker');

var count=0 
var companies=[];
// get all the companies and count them 
function get(userContext, events, done) {
  companies=userContext.vars.companies 
  count = userContext.vars.companies.length
  return done();
}
// rendomly choose a company for status change
function put(userContext, events, done) {
  var randomIndex = Faker.random.number({
            'min': 0,
            'max': count
        });
  if(companies[randomIndex]){
  userContext.vars.id=companies[randomIndex]._id
  }
  return done();
}