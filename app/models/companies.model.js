// */app/models/companies.model.js*

// ## companies Model

// Note: MongoDB will autogenerate an _id for each companies object created

// Grab the Mongoose module
import mongoose from 'mongoose';
let Schema = mongoose.Schema;
import Locations from './locations.model';


// Create a `schema` for the `companies` object
let companiesSchema = new mongoose.Schema({
  name: { 
    type : String,
    unique:true,
    required : true,
    dropDups: true 
    },
  address:{
    type : String 
    },
  address2:{ 
    type : String
    },
  city:{ 
    type : String 
    },
  state:{ 
    type : String
    },
  zipcode:{ 
    type : String 
    },
  county:{ 
    type : String 
    },
  status:{ 
    type : String,
    default: 'new' 
    },
  dateCreated:{
    type: Date,
    default: Date.now
    },
  locations:[{ 
    type: Schema.Types.ObjectId,
    ref: 'Locations'
    }],
});

// Expose the model so that it can be imported and used in
// the controller (to search, delete, etc.)
export default mongoose.model('Companies', companiesSchema);