// */app/models/locations.model.js*

// ## locations Model

// Note: MongoDB will autogenerate an _id for each locations object created

// Grab the Mongoose module
import mongoose from 'mongoose';
// Create a `schema` for the `locations` object
  let locationsSchema = new mongoose.Schema({
  company: { 
    type : String, 
    },
  type:{
    type : String 
    },
  address:{ 
    type : String
    },
  address2:{ 
    type : String 
    },
  city:{ 
    type : String
    },
  state:{ 
    type : String 
    },
  zipcode:{ 
    type : String 
    },
  county:{ 
    type : String 
    },
  status:{ 
    type : String,
    default:'open' 
    },
  dateCreated:{
    type: Date,
    default: Date.now
    },
});

// Expose the model so that it can be imported and used in
// the controller (to search, delete, etc.)
export default mongoose.model('Locations', locationsSchema);