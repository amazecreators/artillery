// */app/routes/_companies.router.js*

// ## Companies API object

// HTTP Verb  Route                           Description

// GET        /api/companies                  Get all of the companies items with locations
// GET        /api/companies/:companies_id    Get a single companies item by companies item id with locations
// POST       /api/companies                  Create a single companies item
// DELETE     /api/companies/:companies_id    Delete a single companies item
// PUT        /api/companies/:companies_id    Update a companies item with new info

// Load the companies model
import Companies from '../models/companies.model';
import Locations from '../models/locations.model';
// import faker from 'faker';
var faker = require('faker');

export default (app, router) => {

  // ### companies API Routes

  // Define routes for the companies item API

  router.route('/companies')



    // Accessed at POST http://localhost:8080/api/companies

    // Create a companies item
    .post((req, res) => {
      let companyid;
      // ### Create a companies item
      Companies.create({
        name: req.body.name,
        //now faker fake all other records
        address: faker.address.streetAddress(),
        address2: faker.address.secondaryAddress(),
        city: faker.address.city(),
        state: faker.address.state(),
        zipcode: faker.address.zipCode(),
        county: faker.address.county(),
      }, (err, companies) => {
        companyid = companies._id
        if (err)
          return res.send(err);

        // ### Create 10 localities item
        let i = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        i.forEach((element) => {
          Locations.create({
            company: req.body.name,
            //now faker fake all other records
            type: faker.random.word([
              'Warehouse, HeadQuaters, Factory'
            ]),
            address: faker.address.streetAddress(),
            address2: faker.address.secondaryAddress(),
            city: faker.address.city(),
            state: faker.address.state(),
            zipcode: faker.address.zipCode(),
            county: faker.address.county(),
          }, (err, locations) => {
            if (locations) {
              if (companyid) {
                Companies.findByIdAndUpdate(companyid, {
                  $push: { locations: locations._id }
                }, (err) => {
                  if (err) {
                    return res.send(err);
                  }
                });
              }
            }
            if (err) {
              return res.send(err);
            }
          });
        })
        // DEBUG
        console.log(`companies created: ${companies}`);
        res.json('Company Created');
      });

    })

    // ### Get all of the companies items

    // Accessed at GET http://localhost:8080/api/companies
    .get((req, res) => {

      // Use mongoose to get all companies items in the database

      Companies.find({}).populate('locations').exec(function (err, companies) {
        if (err)
          res.send(err);

        else
          res.json(companies);
      });

    });

  router.route('/companies/:companies_id')

    // ### Get a companies item by ID

    // Accessed at GET http://localhost:8080/api/companies/:companies_id
    .get((req, res) => {

      // Use mongoose to a single companies item by id in the database
      Companies.findOne(req.params.companies_id, (err, companies) => {

        if (err)
          res.send(err);

        else
          res.json(companies);
      });
    })

    // ### Update a companies item by ID

    // Accessed at PUT http://localhost:8080/api/companies/:companies_id
    .put((req, res) => {

      console.log("hello", req.params.companies_id);
      // Changeing companies statuses
      Companies.findByIdAndUpdate(req.params.companies_id, { $set: { status: 'old' } }, (err, companies) => {
        if (err) {
          return res.send(err);
        }
        let i = faker.random.number({'min': 0,'max': companies.locations.length});
        console.log(companies.locations[i]);
        // Changeing location randomly statuses
        Locations.findByIdAndUpdate(companies.locations[i], { status: 'closed' }, (err) => {
          if (err) {
            return res.send(err);
          }
          // sending respnose
          res.json(companies);
        });
      });


    })

    // ### Delete a companies item by ID

    // Accessed at DELETE http://localhost:8080/api/companies/:companies_id
    .delete((req, res) => {

      // DEBUG
      console.log(`Attempting to delete companies with id: ${req.params.companies_id}`);

      Companies.remove({

        _id: req.params.companies_id
      }, (err, companies) => {

        if (err)
          res.send(err);

        console.log('companies successfully deleted!');

        Companies.find({}).populate('locations').exec(function (err, companies) {
          if (err)
            res.send(err);

          else
            res.json(companies);
        });
      });
    });
};

