// *src/app/app.ts*

// This file contains the main class as well as the necessary
// decorators for creating the primary `app` `component`

/*
 * Angular 2 decorators and services
 */
import { Component } from 'angular2/core';
import { RouteConfig, Router } from 'angular2/router';

//import home component
import { Home } from './home/home.component';

// Import NgFor directive
import { NgFor } from 'angular2/common';


// Import Recipes component

/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app',
  providers: [],
  directives: [
    NgFor,
    Home
  ],
  pipes: [],
  // Load our main `Sass` file into our `app` `component`
  styleUrls: [require('!style!css!sass!../sass/main.scss')],
  template: `
    <header>
      <nav>
        <h1>{{ name }}</h1>
        <ul>
          <li router-active>
            <a [routerLink]=" ['Home'] ">Home</a>
          </li>
        </ul>
      </nav>
    </header>

    <main>
      <router-outlet></router-outlet>
    </main>

  `
})
@RouteConfig([
  { path: '/', name: 'Home', component: Home, useAsDefault: true },
  { path: '/Home', name: 'Home', component: Home },
  // Async load a component using Webpack's require with
  // es6-promise-loader and webpack `require`
])
export class App {
  angularLogo = 'assets/img/angular-logo.png';
  name = 'Faker Artillery automation project';
  url = 'https://twitter.com/datatype_void';

  constructor() {

  }
}

/*
 * Please review the https://github.com/datatype_void/angular2-examples/ repo as it is updated for
 * more angular app examples that you may copy/paste
 * (The examples may not be updated as quickly. Please open an issue on github for us to update it)
 * For help or questions please contact us at @datatype_void on twitter
 * or our chat on Slack at https://VulgarDisplayOfPower.com/slack-join
 * or via chat on Gitter at https://gitter.im/datatype_void/angular2-webpack-starter
 */
