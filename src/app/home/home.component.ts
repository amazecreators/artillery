import { Component, View } from 'angular2/core';

import { HomeService } from './home.service';


// We `import` `http` into our `companiesService` but we can only
// specify providers within our component
import { HTTP_PROVIDERS } from 'angular2/http';

// Import NgFor directive
import { NgFor } from 'angular2/common';

// Create metadata with the `@Component` decorator
@Component({
  // HTML tag for specifying this component
  selector: 'home',
  // Let Angular 2 know about `Http` and `companiesService`
  providers: [...HTTP_PROVIDERS, HomeService]
})
@View({
  template: require('./home.component.html'),
})

export class Home {
  // Initialize our `companiesData.name` to an empty `string`
  companiesData = {
    name: ''
  };
  locations = [];
  companyName = '';
  errorMassage = '';
  private companies: Array<Home> = [];

  constructor(public homeService: HomeService) {
    console.log('Home constructor go!');
    this.getAllCompanies();
  }

  getAllCompanies() {

    //this.companiess = [];
    this.homeService.getAll()
      // `Rxjs`; we subscribe to the response
      .subscribe((res) => {

        // Populate our `companies` array with the `response` data
        this.companies = res;
        // Reset `companies` input
        this.companiesData.name = '';
      });
  }

  createCompany() {
    if (this.companiesData.name != '') {
      this.homeService.createCompanies(this.companiesData)
        .subscribe((res) => {
          if (res.errmsg) {
            this.errorMassage = 'Duplicate Company name';
            setTimeout(() => {
              this.errorMassage = '';
            }, 3000);
          } else {
            console.log(res)
            this.getAllCompanies();
          }
          this.companiesData.name = '';
        });
    }
  }

  showLocations(locations, name) {
    this.locations = locations;
    this.companyName = name;
    let elmnt = document.getElementById("location-list");
    elmnt.scrollIntoView();
  }

  deleteCompany(id) {

    this.homeService.deleteCompanies(id)
      .subscribe((res) => {

        // Populate our `companies` array with the `response` data
        this.companies = res;
      });
  }

}
