import {Injectable} from 'angular2/core';
import {Http, Headers} from 'angular2/http';

// `Injectable` is usually used with `Dart` metadata
// generation; it has no special meaning within `TypeScript`
// This makes sure `TypeScript` emits the needed metadata
// Reference : http://blog.thoughtram.io/angular/2015/09/17/resolve-service-dependencies-in-angular-2.html
@Injectable()
export class HomeService {
  // The `public` keyword denotes that the constructor parameter will
  // Here we intend the constructor function to be called with the
  // `Http` parameter
  constructor(public http:Http) {

  }

  getAll() {
      return this.http.get('/api/companies')
          // map the `HTTP` response from `raw` to `JSON` format
          // using `RxJs`
          // Reference: https://github.com/Reactive-Extensions/RxJS
          .map(res => res.json());
  }

  createCompanies(data) {

    let headers = new Headers();

    headers.append('Content-Type', 'application/json');

    return this.http.post('/api/companies', JSON.stringify(data),
          {headers: headers})
        .map(res => res.json());
  }

  deleteCompanies(id) {

      return this.http.delete(`/api/companies/${id}`)
          .map(res => res.json());
  }
}